#!/usr/bin/env bash
# Version 1.1.1 - 07.06.2022
# This script receive path to config file as an argument.
# Then it takes path from the first commented line from config file about where config should exist.
# And make soft link to that place.

#Warning if argument not passed
if [ -z "$1" ]; then
    echo "You should to pass path to config as a parameter to the script"
else

    # make absolute path from relative
    path_to_file=$(readlink -f "$1")
    echo "1st echo $path_to_file"

    #extract path to link from config file
    #path_to_link=$(sed -e "1 s/^#//" "$path_to_file")
    path_to_link=$(head -1 $path_to_file | sed -e "s/^#//")
    echo "2nd echo  $path_to_link"

    #resovle tilde to variable value
    eval path_to_link=$path_to_link
    # make absolute path from relative
    full_path_to_link=$(readlink -f "$path_to_link")
    echo "3rd echo $full_path_to_link"

    #make soft link
    ln -sf $path_to_file $path_to_link
fi
