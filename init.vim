" ~/.config/nvim/init.vim
" переназначить для кириллицы соответствующий ей символ на латинице:
:set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz 
:set number "отображать номера строк"
:set ruler "вкючить колесо прокрутки"
:set showmode "показывать текущий режим"
:set mouse=a "включить мышку"
:set expandtab "пробелы вместо TAB"
:set tabstop=4 "ширина TAB = 4 пробела"
:set hlsearch "подсветка поисковых запросов в тексте"
:set incsearch "инкрементальный поиск: первого вхождения при поиске не дожидаясь конца ввода"
:syntax on "включить подсветку синтаксиса"
:map <C-n> :NERDTreeToogle<CR> "замапить выполнение :NERDTreeToogle на Ctrl+N"
:inoremap lk <Esc> "назначить Esc на l+k для выхода из режима Вставки"
" :set [no]wildmenu "При авто-дополнении в командной строке над ней выводятся возможные варианты"
:set list "Отображать табуляцию и переводы строк"

